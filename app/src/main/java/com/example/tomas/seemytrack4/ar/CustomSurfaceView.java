package com.example.tomas.seemytrack4.ar;

import android.content.Context;
import android.graphics.PixelFormat;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.opengl.GLSurfaceView;
import android.widget.Toast;

import com.example.tomas.seemytrack4.fragment.ARFragment;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Arrays;

import static android.hardware.SensorManager.AXIS_X;
import static android.hardware.SensorManager.AXIS_Z;
import static android.hardware.SensorManager.getAngleChange;
import static android.hardware.SensorManager.getInclination;
import static android.view.Surface.ROTATION_0;
import static android.view.Surface.ROTATION_180;
import static android.view.Surface.ROTATION_270;
import static android.view.Surface.ROTATION_90;

/**
 * Created by tomas on 15.12.2016.
 */

public class CustomSurfaceView extends GLSurfaceView implements SensorEventListener {

    CustomRenderer mRenderer;

    private float[] mRIdentityMatrix = new float[9];     //R is the identity matrix when the device is aligned with the world's coordinate system, that is, when the device's X axis points toward East, the Y axis points to the North Pole and the device is facing the sky.

    private float[] mOrientationValues = new float[3];
    private float[] mAccelerometerValues = new float[3];
    private float[] mMagneticValues = new float[3];

    private float[] mRotationVectorValues = new float[5];
    private float[] mRotationMatrixFromVector = new float[3];

    private float[] mRotationValues = new float[3];

    private SensorManager mSensors;
    private Sensor mAccelerationSensor;
    private Sensor mMagneticSensor;
    private Sensor mRotationVectorSensor;

    private ARFragment arFragment;

    static final float ALPHA = 0.04f; //????????????????

    private float[] mIRotationMatrix = new float[9];    //I is a rotation matrix transforming the geomagnetic vector into the same coordinate space as gravity (the world's coordinate space). I is a simple rotation around the X axis
    private float[] tmp = new float[9];

    public CustomSurfaceView(Context context) {
        super(context);

        setEGLConfigChooser(8, 8, 8, 8, 16, 0);
        getHolder().setFormat(PixelFormat.TRANSPARENT);
        setEGLContextClientVersion(2);
        mRenderer = new CustomRenderer();
        setRenderer(mRenderer);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        mSensors = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        mAccelerationSensor = mSensors.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mMagneticSensor = mSensors.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        mRotationVectorSensor = mSensors.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);

        mSensors.registerListener(this, mAccelerationSensor, SensorManager.SENSOR_DELAY_UI);
        mSensors.registerListener(this, mMagneticSensor, SensorManager.SENSOR_DELAY_UI);
        mSensors.registerListener(this, mRotationVectorSensor, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        switch (sensorEvent.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                mAccelerometerValues = lowPass(sensorEvent.values.clone(), mAccelerometerValues);
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                mMagneticValues = lowPass(sensorEvent.values.clone(), mMagneticValues);
                break;
            case Sensor.TYPE_ROTATION_VECTOR:
                mRotationVectorValues = lowPass(sensorEvent.values.clone(), mRotationVectorValues);
        }

        if(mAccelerometerValues != null && mMagneticValues != null) {
            boolean success = SensorManager.getRotationMatrix(mRIdentityMatrix, mIRotationMatrix, mAccelerometerValues, mMagneticValues);
            if(success) {
                SensorManager.remapCoordinateSystem(mRIdentityMatrix, AXIS_X, AXIS_Z, tmp); // this helps (y)
                SensorManager.getOrientation(tmp, mOrientationValues);

                mRotationValues[0] = (int) roundArrayValues((float) Math.toDegrees(mOrientationValues[0])); // Azimuth, angle of rotation about the -z axis
                mRotationValues[1] = (int) roundArrayValues((float) Math.toDegrees(mOrientationValues[1])); // Pitch, angle of rotation about the x axis
                mRotationValues[2] = (int) roundArrayValues((float) Math.toDegrees(mOrientationValues[2])); // Roll, angle of rotation about the y axis
                System.out.println(mOrientationValues[0] + " " + mOrientationValues[1] + " " + mOrientationValues[2]);

                getMobileOrientation();
                this.invalidate();
            }
        }
        if(mRotationVectorValues != null) {
            SensorManager.getRotationMatrixFromVector(mRotationMatrixFromVector, mRotationVectorValues);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    private void getMobileOrientation() {
//        mRenderer.mDeltaY = mRotationDelta[0];

//        mRenderer.mDeltaX = mRotationDelta[1];

//        mRenderer.mDeltaZ = mRotationDelta[1];

//        System.out.println(mRenderer.mDeltaY);

        arFragment.showValues(mRotationValues, mRotationVectorValues, mRotationMatrixFromVector);

//        requestRender();
    }

    public void setArFragment(ARFragment arFragment) {
        this.arFragment = arFragment;
    }

    private float roundArrayValues(float value) {
        float tmp = 0;
        tmp = ((int)(value * 100));
        tmp /= 100;

        return tmp;
    }

    protected float[] lowPass(float[] input, float[] output) {
        if(output == null)
            return input;
        for(int i=0; i<input.length; i++) {
            output[i] = output[i] + ALPHA * (input[i] - output[i]);
        }
        return output;
    } //See more at: https://www.built.io/blog/applying-low-pass-filter-to-android-sensor-s-readings#sthash.xd1JROb3.dpuf
}
