package com.example.tomas.seemytrack4.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.tomas.seemytrack4.R;

public class LoginActivity extends AppCompatActivity {
    private EditText mUserName;
    private EditText mUserPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initialization();

        Button logIn = (Button) findViewById(R.id.log_in_button);
        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkLogInData())
                    successfullyLoggedIn();
            }
        });

        Button facebookLogIn = (Button) findViewById(R.id.facebook_log_in_button);
        facebookLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        ImageView logInBackground = (ImageView) findViewById(R.id.log_in_background);
        logInBackground.setScaleType(ImageView.ScaleType.CENTER_CROP);
//        Bitmap backgroundBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.log_in_background); /* TODO: java.lang.OutOfMemoryError !!!!!!! */
        Bitmap backgroundBitmap = decodeSampledBitmapFromResource(getResources(), R.drawable.log_in_background, getScreenHeight(), getScreenHeight());
        logInBackground.setImageBitmap(Bitmap.createScaledBitmap(backgroundBitmap, calculateImageWidth(backgroundBitmap), calculateImageHeight(), true));
        logInBackground.setAnimation(AnimationUtils.loadAnimation(this, R.anim.log_in_background_infinity_scale));
    }

    private void initialization() {
        mUserName = (EditText) findViewById(R.id.name_or_email);
        mUserPassword = (EditText) findViewById(R.id.password);
    }


    private boolean checkLogInData() {
        if(mUserName.getText().toString().length() == 0) {
            mUserName.setError(getString(R.string.log_in_name_error_empty));
            return false;
        }
        if(mUserPassword.getText().toString().length() == 0) {
            mUserPassword.setError(getString(R.string.log_in_password_error_empty));
            return false;
        }

        /* TODO: Check data */
        return true;
    }

    private void successfullyLoggedIn() {
//        Intent intent = new Intent(LoginActivity.this, ApplicationMainActivity.class);
//        startActivity(intent);
//        finish();
    }

    private int calculateImageWidth(Bitmap imageBitmap) {
        return (getScreenHeight() * imageBitmap.getWidth()) / imageBitmap.getHeight();
    }

    private int calculateImageHeight() {
        return getScreenHeight();
    }

    private int getScreenHeight() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        return size.y;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }
}
