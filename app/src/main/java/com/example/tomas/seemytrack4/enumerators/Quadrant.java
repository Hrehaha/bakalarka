package com.example.tomas.seemytrack4.enumerators;

/**
 * Created by tomas on 25.1.2017.
 */

public enum Quadrant {
    // Object position, if user is center of Cartesian coordinate system
    FIRST_QUADRANT,
    SECOND_QUADRANT,
    THIRD_QUADRANT,
    FOURTH_QUADRANT,

    BETWEEN_FIRST_SECOND_QUADRANT, // lies on y-axis, over x-axis
    BETWEEN_FIRST_FOURTH_QUADRANT, // lies on x-axis, right from y-axis
    BETWEEN_SECOND_THIRD_QUADRANT, // lies on x-axis, left from y-axis
    BETWEEN_THIRD_FOURTH_QUADRANT, // lies on y-axis, under x-axis

    VERTICAL_FIRST_QUADRANT,
    VERTICAL_SECOND_QUADRANT,
    VERTICAL_THIRD_QUADRANT,
    VERTICAL_FOURTH_QUADRANT,

    UNSET
}
