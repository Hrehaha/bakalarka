package com.example.tomas.seemytrack4.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tomas.seemytrack4.R;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by tomas on 16.12.2016.
 */

public class ProfileFragment extends Fragment {

    private View mRootView;
    private Context mActivityContext;
    private Activity mParentActivity;

    private TextView textView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        mRootView = inflater.inflate(R.layout.fragment_profile, container, false);
        mActivityContext = this.getContext();
        mParentActivity = this.getActivity();

        return mRootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        textView = (TextView) mRootView.findViewById(R.id.textView3);

        /* TODO:
        pride mi cislo
        zaokruhlit cislo
        vypocitat deltu so starou hodnotou (na zaciatku je stara ta co prisla)
        
         */
    }
}
