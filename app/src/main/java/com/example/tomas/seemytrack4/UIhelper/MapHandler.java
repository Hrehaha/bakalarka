package com.example.tomas.seemytrack4.UIhelper;

import android.graphics.Color;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

/**
 * Created by tomas on 13.12.2016.
 */

public class MapHandler {

    private GoogleMap mMap;
    private Polyline mPolyline;
    private PolylineOptions mPolylineOptions;

    public MapHandler(GoogleMap googleMap) {
        this.mMap = googleMap;

        mPolylineOptions = new PolylineOptions();
        mPolylineOptions
                .width(20)
                .color(Color.RED);

        ArrayList<LatLng> coordList = new ArrayList<LatLng>();
        coordList.add(new LatLng(49.207106, 16.660226));

        coordList.add(new LatLng(49.208000, 16.660227));
        coordList.add(new LatLng(49.208000, 16.661400));
        coordList.add(new LatLng(49.207084, 16.661381));
        coordList.add(new LatLng(49.207106, 16.660226));

        updatePolyline(coordList);

        showTrack();


        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(49.207106, 16.660226), 20));
    }

    private void updatePolyline(ArrayList<LatLng> coordList) {
        mPolylineOptions.addAll(coordList);
    }

    private void showTrack() {
        mPolyline = mMap.addPolyline(mPolylineOptions);

    }
}
