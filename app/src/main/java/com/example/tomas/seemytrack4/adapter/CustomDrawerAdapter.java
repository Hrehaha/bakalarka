package com.example.tomas.seemytrack4.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tomas.seemytrack4.R;
import com.example.tomas.seemytrack4.model.DrawerItem;

import java.util.List;

/**
 * Created by tomas on 13.12.2016.
 */

public class CustomDrawerAdapter extends ArrayAdapter<DrawerItem> {

    private Context context;
    private List<DrawerItem> drawerItemList;
    private int layoutResID;

    public CustomDrawerAdapter(Context context, int layoutResourceID,
                               List<DrawerItem> listItems) {
        super(context, layoutResourceID, listItems);
        this.context = context;
        this.drawerItemList = listItems;
        this.layoutResID = layoutResourceID;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        DrawerItemHolder drawerHolder;
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            drawerHolder = new DrawerItemHolder();

            view = inflater.inflate(layoutResID, parent, false);
            drawerHolder.ItemName = (TextView) view
                    .findViewById(R.id.drawer_itemName);
            drawerHolder.icon = (ImageView) view.findViewById(R.id.drawer_icon);
            drawerHolder.divider = (View) view.findViewById(R.id.dividerList);

            view.setTag(drawerHolder);

        } else {
            drawerHolder = (DrawerItemHolder) view.getTag();

        }

        DrawerItem dItem = (DrawerItem) this.drawerItemList.get(position);
        if (dItem.getImgResID() != 0) {
            drawerHolder.icon.setImageDrawable(view.getResources().getDrawable(
                    dItem.getImgResID()));
        } else {
            drawerHolder.icon.setVisibility(View.GONE);
        }
        if (!dItem.isDivider()) {
            drawerHolder.divider.setVisibility(View.GONE);
        }
        drawerHolder.ItemName.setText(dItem.getName());


        return view;
    }

    private static class DrawerItemHolder {
        TextView ItemName;
        ImageView icon;
        View divider;
    }
}
