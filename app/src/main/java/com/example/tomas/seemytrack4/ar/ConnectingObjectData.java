package com.example.tomas.seemytrack4.ar;

import com.example.tomas.seemytrack4.enumerators.Quadrant;
import com.example.tomas.seemytrack4.enumerators.TransitionDirection;

/**
 * Created by tomas on 16.1.2017.
 */

public class ConnectingObjectData {

    /*  VALUES RELATIVE TO EARTH
        This values does not reflect mobile gps position or mobile rotation.
        Init position of 3D object is in center of mobile screen, y axis is parallel to mobile screen, x axis goes from left to right of the screen, z axis goes from screen to user
        (is perpendicular to mobile screen).
        Inclination due to different altitude of gps coordinates is reflected by rotation around x axis, so angle calculated using basic trigonometric equation is applied.
        Different gps positions (one is closer to user and second is further) cause rotation around y axis. This rotation is calculated using Pythagorean theorem and trigonometric equation.
        Result from this formula is geographic azimuth.
        Rotation around z axis is not considered in this values (init rotation around z axis is 0).
        This values simulate situation, when user does not need mobile to see this object. Using this values, we are able to set object into the scene.
        Further changes will consider this init values to reflect absolute position in virtual scene.
     */
    private float mInitLatitudeStart; // in degrees
    private float mInitLongitudeStart; // in degrees
    private float mInitAltitudeStart; // in degrees

    private float mInitLatitudeEnd; // in degrees
    private float mInitLongitudeEnd; // in degrees
    private float mInitAltitudeEnd; // in degrees

    private float mInitLatitudeCenter; // in degrees
    private float mInitLongitudeCenter; // in degrees
    private float mInitAltitudeCenter; // in degrees

    private float mInitAngleXAxis; // in degrees
    private float mInitAngleYAxis; // in degrees
    private float mInitAngleZAxis; // in degrees
    /* END OF VALUES RELATIVE TO EARTH */

    /* VALUES RELATIVE TO MOBILE */
    // Transition only for first gps coordinates
    // This values have to be modify for further use in openGL drawing
    private float mTransitionXAxis; // in m
    private float mTransitionYAxis; // in m
    private float mTransitionZAxis; // in m

    private float mRotationXAxis; // in degrees
    private float mRotationYAxis; // in degrees
    private float mRotationZAxis; // in degrees

    private boolean mIsVisible = true; // optimization value, true if is object in front of user, false if is behind or beneath
    /* END OF VALUES RELATIVE TO MOBILE */

    private float mDistanceBetweenCoordinates; // in m

    private Quadrant mPosition = Quadrant.UNSET;
    private Quadrant mVerticalPosition = Quadrant.UNSET;

    private TransitionDirection mHorizontalTransitionDirection = TransitionDirection.UNSET;
    private TransitionDirection mVerticalTransitionDirection = TransitionDirection.UNSET;

    public ConnectingObjectData() {

    }

    public void doInitMath() {
        mDistanceBetweenCoordinates = (float) calculateDistanceBetweenCoordinates(mInitLatitudeStart, mInitLongitudeStart, mInitLatitudeEnd, mInitLongitudeEnd);

        mInitAngleXAxis = calculateInclination(mInitAltitudeStart, mInitAltitudeEnd,mDistanceBetweenCoordinates);
        mInitAngleYAxis = calculateInitRotationForYAxis();
        mInitAngleZAxis = 0.0f;

        calculateCenterGPSCoordinates();
    }

    /* TODO: Should be private, but for tests it is public for now */
    public double calculateDistanceBetweenCoordinates(double latitudeStart, double longitudeStart, double latitudeEnd, double longitudeEnd) {
        /* TODO: Longest distance biggest mistake, both gps coordinates negative (-latitude && -longitude) big mistake */
        // a = sin²(Δφ/2) + cos φ1 ⋅ cos φ2 ⋅ sin²(Δλ/2)
        // c = 2 ⋅ atan2( √a, √(1−a) )
        // d = R ⋅ c
        // φ is latitude, λ is longitude, R is earth’s radius (mean radius = 6,371km); note that angles need to be in radians to pass to trig functions!
        // http://www.movable-type.co.uk/scripts/latlong.html
        latitudeStart = Math.toRadians(latitudeStart);
        latitudeEnd = Math.toRadians(latitudeEnd);
        longitudeStart = Math.toRadians(longitudeStart);
        longitudeEnd = Math.toRadians(longitudeEnd);

        double deltaLatitude = latitudeEnd - latitudeStart;
        double deltaLongitude = longitudeEnd - longitudeStart;

        double sinDeltaCoordinatesStart = Math.sin(deltaLatitude / 2.0);
        double sinDeltaCoordinatesEnd = Math.sin(deltaLongitude / 2.0);

        double a = Math.pow(sinDeltaCoordinatesStart, 2.0) + Math.cos(latitudeStart) * Math.cos(latitudeEnd) * Math.pow(sinDeltaCoordinatesEnd, 2.0);

        double c = 2.0 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return (6371 * c) * 1000; // to meters
    }

    /* TODO: Should be private, but for tests it is public for now */
    public float calculateInclination(float altitudeStart, float altitudeEnd, float distanceBetweenCoordinates) {
        // Angle against surface define x and y axises
        // Super accurate
        // If is first coordinate higher than second, angle is negative
        double inclination = Math.atan((altitudeEnd - altitudeStart) / distanceBetweenCoordinates); // trigonometry, altitude in m
        return (float) Math.abs(Math.toDegrees(inclination)); /* TODO: Maybe you do not have to change negative to positive value ?? */
    }

    /* TODO: Should be private, but for tests it is public for now */
    public float calculateInitRotationForYAxis() {
        // Rotation around z-axis
        // Switching longitude of second gps position with longitude of first gps position can be calculate side in right triangle
        /* TODO: Check if this switching is really working */
        double side = calculateDistanceBetweenCoordinates(mInitLatitudeStart, mInitLongitudeStart, mInitLatitudeStart, mInitLongitudeEnd);
        double angle = Math.acos(side / mDistanceBetweenCoordinates);
        return (float) Math.toDegrees(angle);
    }

    /* TODO: Should be private, but for tests it is public for now */
    public void calculateAbsoluteTransitionXAxis(float userLatitude, float userLongitude, float userAzimuth) {
        /* TODO: consider some sort of mistake distribution */
        double sceneRadius = calculateDistanceBetweenCoordinates(userLatitude, userLongitude, mInitLatitudeCenter, mInitLongitudeCenter);
        // latitude of user, longitude of center between coordinates
        double triangleSideAgainstUser = calculateDistanceBetweenCoordinates(userLatitude, mInitLongitudeCenter, mInitLatitudeCenter, mInitLongitudeCenter);
        // additional angle
        double gama = Math.asin(triangleSideAgainstUser / sceneRadius);
        // circular cut
        mTransitionXAxis = calculateCircularCut(sceneRadius, calculateAngleForCircularCut(gama, userAzimuth));
    }

    /* TODO: Should be private, but for tests it is public for now */
    public void calculateAbsoluteTransitionYAxis(float userLatitude, float userLongitude, float userAltitude, float mobilePitchAngle) {
        double sceneRadius = calculateDistanceBetweenCoordinates(userLatitude, userLongitude, mInitLatitudeCenter, mInitLongitudeCenter);
        // latitude of user, longitude of center between coordinates
        double triangleSideAgainstUser;
        /* TODO: What if user is right in center of object?? */
        if(mInitAltitudeCenter > userAltitude)
            triangleSideAgainstUser = mInitAltitudeCenter - userAltitude;
        else
            triangleSideAgainstUser = userAltitude - mInitAltitudeCenter;
        // additional angle
        double gama = Math.asin(triangleSideAgainstUser / sceneRadius);
        // circular cut
        mTransitionXAxis = calculateCircularCut(sceneRadius, calculateAngleForCircularCutVerticalShift(gama, mobilePitchAngle));
    }

    /* TODO: Should be private, but for tests it is public for now */
    public float calculateAbsoluteTransitionZAxis(float userLatitude, float userLongitude) {
        /* TODO: Maybe you have to switch coordinates if is user and object in different positions */
        // mTransitionZAxis = calculateDistanceBetweenCoordinates(userLatitude, userLongitude, mInitLatitudeCenter, mInitLongitudeCenter);
        return (float) calculateDistanceBetweenCoordinates(userLatitude, userLongitude, mInitLatitudeCenter, mInitLongitudeCenter);
    }

    /* TODO: Should be private, but for tests it is public for now */
    public void checkVisibility(float userAzimuth, float mobilePitch, float mobileRoll) {
        /* TODO: Based on azimuth and position set false if there is no chance for user to see object */
        /* TODO: If is object in third or fourth quadrant in vertical context set visibility false */
        mIsVisible = true;
    }

    /* TODO: Should be private, but for tests it is public for now */
    public void calculateCenterGPSCoordinates() {
        if(mInitLatitudeStart > mInitLatitudeEnd) {
            mInitLatitudeCenter = mInitLatitudeStart - ((mInitLatitudeStart - mInitLatitudeEnd) / 2);
        } else {
            if(mInitLatitudeEnd > mInitLatitudeStart) {
                mInitLatitudeCenter = mInitLatitudeEnd - ((mInitLatitudeEnd - mInitLatitudeStart) / 2);
            } else {
                mInitLatitudeCenter = mInitLatitudeStart;
            }
        }

        if(mInitLongitudeStart > mInitLongitudeEnd) {
            mInitLongitudeCenter = mInitLongitudeStart - ((mInitLongitudeStart - mInitLongitudeEnd) / 2);
        } else {
            if(mInitLongitudeEnd > mInitLongitudeStart) {
                mInitLongitudeCenter = mInitLongitudeEnd - ((mInitLongitudeEnd - mInitLongitudeStart) / 2);
            } else {
                mInitLongitudeCenter = mInitLongitudeStart;
            }
        }

        if(mInitAltitudeStart > mInitAltitudeEnd) {
            mInitAltitudeCenter = mInitAltitudeStart - ((mInitAltitudeStart - mInitAltitudeEnd) / 2);
        } else {
            if(mInitAltitudeEnd > mInitAltitudeStart) {
                mInitAltitudeCenter = mInitAltitudeEnd - ((mInitAltitudeEnd - mInitAltitudeStart) / 2);
            } else {
                mInitAltitudeCenter = mInitAltitudeStart;
            }
        }
    }

    /* TODO: Should be private, but for tests it is public for now */
    public void determineQuadrantPosition(float userLatitude, float userLongitude, float userAltitude) {
        Quadrant position = Quadrant.UNSET;
        if(userLatitude < mInitLatitudeCenter && userLongitude < mInitLongitudeCenter)
            position = Quadrant.FIRST_QUADRANT;
        if(userLatitude < mInitLatitudeCenter && userLongitude > mInitLongitudeCenter)
            position = Quadrant.SECOND_QUADRANT;
        if(userLatitude > mInitLatitudeCenter && userLongitude > mInitLongitudeCenter)
            position = Quadrant.THIRD_QUADRANT;
        if(userLatitude > mInitLatitudeCenter && userLongitude < mInitLongitudeCenter)
            position = Quadrant.FOURTH_QUADRANT;

        if(userLatitude < mInitLatitudeCenter && userLongitude == mInitLongitudeCenter)
            position = Quadrant.BETWEEN_FIRST_SECOND_QUADRANT;
        if(userLatitude == mInitLatitudeCenter && userLongitude > mInitLongitudeCenter)
            position = Quadrant.BETWEEN_SECOND_THIRD_QUADRANT;
        if(userLatitude > mInitLatitudeCenter && userLongitude == mInitLongitudeCenter)
            position = Quadrant.BETWEEN_THIRD_FOURTH_QUADRANT;
        if(userLatitude == mInitLatitudeCenter && userLongitude < mInitLongitudeCenter)
            position = Quadrant.BETWEEN_FIRST_FOURTH_QUADRANT;

        mPosition = position;

        if(userAltitude < mInitAltitudeCenter) {
            if(position == Quadrant.FIRST_QUADRANT || position == Quadrant.FOURTH_QUADRANT)
                mVerticalPosition = Quadrant.VERTICAL_FIRST_QUADRANT;
            if(position == Quadrant.SECOND_QUADRANT || position == Quadrant.THIRD_QUADRANT)
                mVerticalPosition = Quadrant.VERTICAL_SECOND_QUADRANT;
        }
        if(userAltitude > mInitAltitudeCenter) {
            if(position == Quadrant.FIRST_QUADRANT || position == Quadrant.FOURTH_QUADRANT)
                mVerticalPosition = Quadrant.VERTICAL_FOURTH_QUADRANT;
            if(position == Quadrant.SECOND_QUADRANT || position == Quadrant.THIRD_QUADRANT)
                mVerticalPosition = Quadrant.VERTICAL_THIRD_QUADRANT;
        }
    }

    /* TODO: Should be private, but for tests it is public for now */
    public float calculateAngleForCircularCut(double gama, double azimuth) {
        double beta = 0;
        double absoluteAzimuth = Math.abs(azimuth);
        /* TODO: Set direction of transition */
        switch(mPosition) {
            case FIRST_QUADRANT:
                if(azimuth < 0) {
                    beta = 90 + absoluteAzimuth - gama;
                } else {
                    if((absoluteAzimuth + gama) < 90)
                        beta = 90 - absoluteAzimuth - gama;
                    else
                        beta = absoluteAzimuth + gama - 90;
                }
                break;
            case SECOND_QUADRANT:
                if(azimuth >= 0) {
                    beta = 90 + absoluteAzimuth - gama;
                } else {
                    if((absoluteAzimuth + gama) < 90)
                        beta = 90 - absoluteAzimuth - gama;
                    else
                        beta = absoluteAzimuth + gama - 90;
                }
                break;
            case THIRD_QUADRANT:
                if(azimuth >= 0) {
                    beta = 90 + absoluteAzimuth + gama;
                } else {
                    if((absoluteAzimuth - 90) < gama)
                        beta = 90 - absoluteAzimuth + gama;
                    else
                        beta = absoluteAzimuth - gama - 90;
                }
                break;
            case FOURTH_QUADRANT:
                if(azimuth < 0) {
                    beta = 90 + absoluteAzimuth + gama;
                } else {
                    if((absoluteAzimuth - 90) < gama)
                        beta = 90 - absoluteAzimuth + gama;
                    else
                        beta = absoluteAzimuth - gama - 90;
                }
                break;
            case BETWEEN_FIRST_FOURTH_QUADRANT:
                if(azimuth <= 0) {
                    beta = 90 + absoluteAzimuth;
                } else {
                    if(azimuth < 90) {
                        beta = 90 - absoluteAzimuth;
                    } else {
                        beta = absoluteAzimuth - 90;
                    }
                }
                break;
            case BETWEEN_SECOND_THIRD_QUADRANT:
                if(azimuth >= 0) {
                    beta = 90 + absoluteAzimuth;
                } else {
                    if(azimuth > -90) {
                        beta = 90 - absoluteAzimuth;
                    } else {
                        beta = absoluteAzimuth - 90;
                    }
                }
                break;
            case BETWEEN_THIRD_FOURTH_QUADRANT:
                beta = 180 - absoluteAzimuth;
                break;
            case BETWEEN_FIRST_SECOND_QUADRANT:
                beta = azimuth;
                break;
        }
        // if gama == (differenceAzimuthRoll || absoluteDifferenceAzimuthRoll), then user is looking right on object so there is no transition
        return (float) Math.abs(beta);
    }

    /* TODO: Should be private, but for tests it is public for now */
    public float calculateCircularCut(double radius, double betaAngle) {
        return (float) ((2 * Math.PI * radius * betaAngle) / 360);
    }

    public float calculateAngleForCircularCutVerticalShift(double gama, double pitch) {
        double beta = 0;
        double absolutePitch = Math.abs(pitch);

        switch (mVerticalPosition) { // Same calculations ???
            case VERTICAL_FIRST_QUADRANT:
                if(pitch > 0) {
                    beta = 90 - gama + absolutePitch;
                } else {
                    if(absolutePitch + gama > 90)
                        beta = gama + absolutePitch - 90;
                    else {
                        if(absolutePitch + gama < 90)
                            beta = 90 - absolutePitch - gama;
                    }
                }
                break;
            case VERTICAL_SECOND_QUADRANT:
                if(pitch < 0) {
                    beta = 90 - gama + absolutePitch;
                } else {
                    if(absolutePitch + gama > 90)
                        beta = gama + absolutePitch - 90;
                    else {
                        if(absolutePitch + gama < 90)
                            beta = 90 - absolutePitch - gama;
                    }
                }
                break;
        }

        return (float) Math.abs(beta);
    }

    public float getmDistanceBetweenCoordinates() {
        return mDistanceBetweenCoordinates;
    }

    public void setmDistanceBetweenCoordinates(float mDistanceBetweenCoordinate) {
        this.mDistanceBetweenCoordinates = mDistanceBetweenCoordinate;
    }

    public float getmInitLongitudeStart() {
        return mInitLongitudeStart;
    }

    public void setmInitLongitudeStart(float mInitLongitudeStart) {
        this.mInitLongitudeStart = mInitLongitudeStart;
    }

    public float getmInitLatitudeStart() {
        return mInitLatitudeStart;
    }

    public void setmInitLatitudeStart(float mInitLatitudeStart) {
        this.mInitLatitudeStart = mInitLatitudeStart;
    }

    public float getmInitAltitudeStart() {
        return mInitAltitudeStart;
    }

    public void setmInitAltitudeStart(float mInitAltitudeStart) {
        this.mInitAltitudeStart = mInitAltitudeStart;
    }

    public float getmInitLatitudeEnd() {
        return mInitLatitudeEnd;
    }

    public void setmInitLatitudeEnd(float mInitLatitudeEnd) {
        this.mInitLatitudeEnd = mInitLatitudeEnd;
    }

    public float getmInitLongitudeEnd() {
        return mInitLongitudeEnd;
    }

    public void setmInitLongitudeEnd(float mInitLongitudeEnd) {
        this.mInitLongitudeEnd = mInitLongitudeEnd;
    }

    public float getmInitAltitudeEnd() {
        return mInitAltitudeEnd;
    }

    public void setmInitAltitudeEnd(float mInitAltitudeEnd) {
        this.mInitAltitudeEnd = mInitAltitudeEnd;
    }

    public float getmInitAltitudeCenter() {
        return mInitAltitudeCenter;
    }

    public void setmInitAltitudeCenter(float mInitAltitudeCenter) {
        this.mInitAltitudeCenter = mInitAltitudeCenter;
    }

    public float getmInitLongitudeCenter() {
        return mInitLongitudeCenter;
    }

    public void setmInitLongitudeCenter(float mInitLongitudeCenter) {
        this.mInitLongitudeCenter = mInitLongitudeCenter;
    }

    public float getmInitLatitudeCenter() {
        return mInitLatitudeCenter;
    }

    public void setmInitLatitudeCenter(float mInitLatitudeCenter) {
        this.mInitLatitudeCenter = mInitLatitudeCenter;
    }

    public float getmInitAngleXAxis() {
        return mInitAngleXAxis;
    }

    public void setmInitAngleXAxis(float mInitAngleXAxis) {
        this.mInitAngleXAxis = mInitAngleXAxis;
    }

    public float getmInitAngleYAxis() {
        return mInitAngleYAxis;
    }

    public void setmInitAngleYAxis(float mInitAngleYAxis) {
        this.mInitAngleYAxis = mInitAngleYAxis;
    }

    public float getmInitAngleZAxis() {
        return mInitAngleZAxis;
    }

    public void setmInitAngleZAxis(float mInitAngleZAxis) {
        this.mInitAngleZAxis = mInitAngleZAxis;
    }

    public float getmTransitionXAxis() {
        return mTransitionXAxis;
    }

    public void setmTransitionXAxis(float mTransitionXAxis) {
        this.mTransitionXAxis = mTransitionXAxis;
    }

    public float getmTransitionYAxis() {
        return mTransitionYAxis;
    }

    public void setmTransitionYAxis(float mTransitionYAxis) {
        this.mTransitionYAxis = mTransitionYAxis;
    }

    public float getmTransitionZAxis() {
        return mTransitionZAxis;
    }

    public void setmTransitionZAxis(float mTransitionZAxis) {
        this.mTransitionZAxis = mTransitionZAxis;
    }

    public float getmRotationXAxis() {
        return mRotationXAxis;
    }

    public void setmRotationXAxis(float mRotationXAxis) {
        this.mRotationXAxis = mRotationXAxis;
    }

    public float getmRotationYAxis() {
        return mRotationYAxis;
    }

    public void setmRotationYAxis(float mRotationYAxis) {
        this.mRotationYAxis = mRotationYAxis;
    }

    public float getmRotationZAxis() {
        return mRotationZAxis;
    }

    public void setmRotationZAxis(float mRotationZAxis) {
        this.mRotationZAxis = mRotationZAxis;
    }

    public boolean ismIsVisible() {
        return mIsVisible;
    }

    public void setmIsVisible(boolean mIsVisible) {
        this.mIsVisible = mIsVisible;
    }

    public Quadrant getmPosition() {
        return mPosition;
    }

    public void setmPosition(Quadrant mPosition) {
        this.mPosition = mPosition;
    }
}
