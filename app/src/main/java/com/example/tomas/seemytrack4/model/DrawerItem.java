package com.example.tomas.seemytrack4.model;

/**
 * Created by tomas on 13.12.2016.
 */

public class DrawerItem {

    private String name;
    private int imgResID;
    private boolean divider;


    @Override
    public String toString() {
        return "DrawerItem{" +
                "name='" + name + '\'' +
                ", imgResID=" + imgResID +
                ", divider=" + divider +
                '}';
    }

    public DrawerItem(String name, int imgResID, boolean divider) {
        this.name = name;
        this.imgResID = imgResID;
        this.divider = divider;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImgResID() {
        return imgResID;
    }

    public void setImgResID(int imgResID) {
        this.imgResID = imgResID;
    }

    public boolean isDivider() {
        return divider;
    }

    public void setDivider(boolean divider) {
        this.divider = divider;
    }
}
