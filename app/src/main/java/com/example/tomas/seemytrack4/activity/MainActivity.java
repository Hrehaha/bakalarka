package com.example.tomas.seemytrack4.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.tomas.seemytrack4.Client;
import com.example.tomas.seemytrack4.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        new Client(this);

        Intent intent = new Intent(MainActivity.this, ApplicationMainActivity.class);
        startActivity(intent);
        finish();
    }
}
