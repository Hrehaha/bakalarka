package com.example.tomas.seemytrack4.enumerators;

/**
 * Created by tomas on 27.1.2017.
 */

public enum TransitionDirection {
    SHIFT_RIGHT,
    SHIFT_LEFT,
    SHIFT_UP,
    SHIFT_DOWN,
    SHIFT_FORWARD,
    SHIFT_BACKWAD,
    UNSET
}
