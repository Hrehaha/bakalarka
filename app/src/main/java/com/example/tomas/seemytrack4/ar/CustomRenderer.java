package com.example.tomas.seemytrack4.ar;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by tomas on 15.12.2016.
 */

public class CustomRenderer implements GLSurfaceView.Renderer {

    volatile public float mDeltaX, mDeltaY, mDeltaZ;

    float[] m_fProjMatrix = new float[16];
    float[] m_fViewMatrix = new float[16];
    float[] m_fIdentity = new float[16];
    float[] m_fVPMatrix = new float[16];
    /** Store the accumulated rotation. */
    private float[] mAccumulatedRotation = new float[16];
    /** Store the current rotation. */
    private float[] mCurrentRotation = new float[16];
    /** A temporary matrix */
    private float[] mTemporaryMatrix = new float[16];

    private TemporaryCube cube;

    @Override
    public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {
        cube = new TemporaryCube();

        GLES20.glClearColor(0, 0, 0, 0);
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        GLES20.glDepthFunc(GLES20.GL_LEQUAL);
        GLES20.glFrontFace(GLES20.GL_CCW);
        GLES20.glEnable(GLES20.GL_CULL_FACE);
        GLES20.glCullFace(GLES20.GL_BACK);
        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

        Matrix.setLookAtM(m_fViewMatrix, 0, 0, 0, 6, 0, 0, 0, 0, 1, 0);
        Matrix.setIdentityM(mAccumulatedRotation, 0);
    }

    @Override
    public void onSurfaceChanged(GL10 gl10, int width, int height) {
        GLES20.glViewport(0, 0, width, height);
        Matrix.frustumM(m_fProjMatrix, 0, -(float)width/height, (float)width/height, -1, 1, 1, 10);
    }

    @Override
    public void onDrawFrame(GL10 gl10) {
//        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
//
//        Matrix.setIdentityM(m_fIdentity, 0);
//        Matrix.setIdentityM(mCurrentRotation, 0);
//
////        Matrix.scaleM(mCurrentRotation, 0, 1.0f, 1.0f, 1.0f);
//
//        Matrix.rotateM(mCurrentRotation, 0, mDeltaX, 1.0f, 0.0f, 0.0f);
//        Matrix.rotateM(mCurrentRotation, 0, mDeltaY, 0.0f, 1.0f, 0.0f);
//        Matrix.rotateM(mCurrentRotation, 0, mDeltaZ, 0.0f, 0.0f, 1.0f);
//
////        Matrix.translateM(mCurrentRotation, 0, 0.0f, 0.0f, 0.0f);
//
//        mDeltaX = 0.0f;
//        mDeltaY = 0.0f;
//        mDeltaZ = 0.0f;
//        Matrix.multiplyMM(mTemporaryMatrix, 0, mCurrentRotation, 0, mAccumulatedRotation, 0);
//        System.arraycopy(mTemporaryMatrix, 0, mAccumulatedRotation, 0, 16);
//        Matrix.multiplyMM(mTemporaryMatrix, 0, m_fIdentity, 0, mAccumulatedRotation, 0);
//        System.arraycopy(mTemporaryMatrix, 0, m_fIdentity, 0, 16);
//
//        Matrix.multiplyMM(m_fVPMatrix, 0, m_fViewMatrix, 0, m_fIdentity, 0);
//        Matrix.multiplyMM(m_fVPMatrix, 0, m_fProjMatrix, 0, m_fVPMatrix, 0);
//
//
//        cube.draw(m_fVPMatrix);
    }
}
