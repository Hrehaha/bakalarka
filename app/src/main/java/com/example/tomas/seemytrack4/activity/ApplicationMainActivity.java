package com.example.tomas.seemytrack4.activity;

import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.example.tomas.seemytrack4.R;
import com.example.tomas.seemytrack4.adapter.CustomDrawerAdapter;
import com.example.tomas.seemytrack4.fragment.ARFragment;
import com.example.tomas.seemytrack4.fragment.MapsFragment;
import com.example.tomas.seemytrack4.fragment.ProfileFragment;
import com.example.tomas.seemytrack4.model.DrawerItem;
import com.google.android.gms.maps.MapFragment;

import java.util.ArrayList;
import java.util.List;

public class ApplicationMainActivity extends AppCompatActivity {

    private DrawerLayout mDrawer;
    private boolean userLoggedIn = true;
    private FloatingActionButton switchMainFragments;

    private boolean isMap = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_application_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.drawer_open, R.string.drawer_close);
        mDrawer.setDrawerListener(toggle);
        toggle.syncState();

        final Fragment mapFragment = new MapsFragment();
        final Fragment arFragment = new ARFragment();

//        getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_placeholder, mapFragment)
                .commit();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.ar_placeholder, arFragment)
                .commit();

        switchMainFragments = (FloatingActionButton) findViewById(R.id.switch_main_fragments);
        switchMainFragments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isMap)
                    switchMainFragments(arFragment);
                else
                    switchMainFragments(mapFragment);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        showNavigationDrawerList();
    }

    private void showNavigationDrawerList() {
        ListView drawerList = (ListView) findViewById(R.id.drawer_list);
        List<DrawerItem> drawerItems = new ArrayList<>();

        if(userLoggedIn) {
            drawerItems.add(new DrawerItem(getString(R.string.profile), R.drawable.ic_account_circle_green_24dp, true));
            drawerItems.add(new DrawerItem(getString(R.string.tracks), R.drawable.ic_directions_walk_green_24dp, false));
            drawerItems.add(new DrawerItem(getString(R.string.scheduler), R.drawable.ic_assignment_green_24dp, false));
            drawerItems.add(new DrawerItem(getString(R.string.statistics), R.drawable.ic_insert_chart_green_24dp, true));
            drawerItems.add(new DrawerItem(getString(R.string.friends), R.drawable.ic_perm_contact_calendar_green_24dp, false));
            drawerItems.add(new DrawerItem(getString(R.string.events), R.drawable.ic_close_white_24dp, false));
            drawerItems.add(new DrawerItem(getString(R.string.settings), R.drawable.ic_settings_green_24dp, true));
            drawerItems.add(new DrawerItem(getString(R.string.log_out), R.drawable.ic_lock_outline_green_24dp, false));
        } else {
            drawerItems.add(new DrawerItem(getString(R.string.log_in), R.drawable.ic_lock_open_green_24dp, false));
        }

        drawerList.setOnItemClickListener(new OnDrawerItemClickListener());

        CustomDrawerAdapter adapter = new CustomDrawerAdapter(this, R.layout.item_drawer, drawerItems);
        drawerList.setAdapter(adapter);
    }

    private void switchMainFragments(Fragment fragment) {
        if(isMap) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.ar_placeholder, fragment)
                    .commit();
            switchMainFragments.setImageResource(R.drawable.ic_map_white_24dp);
            isMap = false;
            ((FrameLayout) findViewById(R.id.ar_placeholder)).setVisibility(View.VISIBLE);
            ((FrameLayout) findViewById(R.id.fragment_placeholder)).setVisibility(View.GONE);
            ((FrameLayout) findViewById(R.id.fragment_placeholder)).startAnimation(AnimationUtils.loadAnimation(this, R.anim.main_fragment_from_visible_to_left_gone));
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
//                    .setCustomAnimations(R.anim.main_fragment_from_gone_left_to_right_visible, R.anim.main_fragment_from_visible_to_right_gone)
                    .replace(R.id.fragment_placeholder, fragment)
                    .commit();
            switchMainFragments.setImageResource(R.drawable.ic_videocam_white_24dp);
            isMap = true;
            ((FrameLayout) findViewById(R.id.fragment_placeholder)).setVisibility(View.VISIBLE);
            ((FrameLayout) findViewById(R.id.fragment_placeholder)).startAnimation(AnimationUtils.loadAnimation(this, R.anim.main_fragment_from_gone_left_to_right_visible));
            ((FrameLayout) findViewById(R.id.ar_placeholder)).setVisibility(View.GONE);
        }
    }

    private class OnDrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            switch (position) {
                case 0:
                    /* TODO: Set white icon, set green to others */
                    switchFragment(new ProfileFragment());
                    break;
            }
            mDrawer.closeDrawer(GravityCompat.START);
        }
    }

    private void switchFragment(Fragment fragment) {
        ((FrameLayout) findViewById(R.id.ar_placeholder)).setVisibility(View.GONE);
        switchMainFragments.setVisibility(View.GONE);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_placeholder, fragment)
                .commit();
    }
}
