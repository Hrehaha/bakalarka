package com.example.tomas.seemytrack4.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.tomas.seemytrack4.R;
import com.example.tomas.seemytrack4.ar.CustomSurfaceView;
import com.example.tomas.seemytrack4.camera.CameraView;

/**
 * Created by tomas on 15.12.2016.
 */

public class ARFragment extends Fragment {

    private View mRootView;
    private Context mActivityContext;
    private Activity mParentActivity;
    private CustomSurfaceView customSurfaceView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        mRootView = inflater.inflate(R.layout.fragment_ar, container, false);
        mActivityContext = this.getContext();
        mParentActivity = this.getActivity();

        return mRootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FrameLayout cameraView = (FrameLayout) mRootView.findViewById(R.id.camera_view);
        customSurfaceView = new CustomSurfaceView(mActivityContext);
        cameraView.addView(customSurfaceView);
        cameraView.addView(new CameraView(mActivityContext, mParentActivity));

        customSurfaceView.setArFragment(this);
    }

    public void showValues(float[] actualRotationMatrix, float[] rotationVectorValues, float[] mRotationMatrixFromVector) {
        ((TextView) mRootView.findViewById(R.id.azimuth)).setText(String.valueOf(actualRotationMatrix[0]));
        ((TextView) mRootView.findViewById(R.id.pitch)).setText(String.valueOf(actualRotationMatrix[1]));
        ((TextView) mRootView.findViewById(R.id.roll)).setText(String.valueOf(actualRotationMatrix[2]));

        ((TextView) mRootView.findViewById(R.id.azimuth_minus_roll)).setText(String.valueOf(Math.toDegrees(rotationVectorValues[0])) + "\n" +
                String.valueOf(Math.toDegrees(rotationVectorValues[1])) + "\n" +
                String.valueOf(Math.toDegrees(rotationVectorValues[2])) + "\n" +
                String.valueOf(Math.toDegrees(rotationVectorValues[3])) + "\n" +
                String.valueOf(Math.toDegrees(rotationVectorValues[4])));


        ((TextView) mRootView.findViewById(R.id.pitch_minus_roll)).setText(String.valueOf(mRotationMatrixFromVector[0]) + "\n" +
                String.valueOf(mRotationMatrixFromVector[1]) + "\n" +
                String.valueOf(mRotationMatrixFromVector[2]));
        ((TextView) mRootView.findViewById(R.id.azimuth_minus_pitch)).setText("eulerRoll:");
    }
}
