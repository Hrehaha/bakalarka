package com.example.tomas.seemytrack4.test;

import com.example.tomas.seemytrack4.ar.ConnectingObjectData;
import com.example.tomas.seemytrack4.enumerators.Quadrant;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by tomas on 27.1.2017.
 */
public class ConnectingObjectDataTest {

    @Test
    public void doInitMath() throws Exception {

    }

    @Test
    public void calculateDistanceBetweenCoordinates() throws Exception {
        System.out.println("Test for calculation distance between two GPS coordinates:");
        ConnectingObjectData connectingObjectData = new ConnectingObjectData();

        System.out.print("    First test:");
        float startLatitude = 49.207993f;
        float startLongitude = 16.660222f;

        float endLatitude = 49.207101f;
        float endLongitude = 16.660211f;

        assertEquals(100.00f, connectingObjectData.calculateDistanceBetweenCoordinates(startLatitude, startLongitude, endLatitude, endLongitude), 1.0f);
        System.out.println(" pass.");

        System.out.print("    Second test:");
        startLatitude = 49.207101f;
        startLongitude = 16.660211f;

        endLatitude = 49.207993f;
        endLongitude = 16.660222f;

        assertEquals(100.00f, connectingObjectData.calculateDistanceBetweenCoordinates(startLatitude, startLongitude, endLatitude, endLongitude), 1.0f);
        System.out.println(" pass.");

        System.out.print("    Third test:");
        startLatitude = -1.338389f;
        startLongitude = 31.815362f;

        endLatitude = -1.340022f;
        endLongitude = 31.813959f;

        assertEquals(240.00f, connectingObjectData.calculateDistanceBetweenCoordinates(startLatitude, startLongitude, endLatitude, endLongitude), 1.0f);
        System.out.println(" pass.");

        System.out.print("    Fourth test:");
        startLatitude = -1.340022f;
        startLongitude = 31.813959f;

        endLatitude = -1.338389f;
        endLongitude = 31.815362f;

        assertEquals(240.00f, connectingObjectData.calculateDistanceBetweenCoordinates(startLatitude, startLongitude, endLatitude, endLongitude), 1.0f);
        System.out.println(" pass.");

        System.out.print("    Fifth test:");
        startLatitude = 36.255130f;
        startLongitude = -120.193421f;

        endLatitude = 36.255136f;
        endLongitude = -120.191843f;

        assertEquals(142.0f, connectingObjectData.calculateDistanceBetweenCoordinates(startLatitude, startLongitude, endLatitude, endLongitude), 1.0f);
        System.out.println(" pass.");

        System.out.print("    Sixth test:");
        startLatitude = 36.255136f;
        startLongitude = -120.191843f;

        endLatitude = 36.255130f;
        endLongitude = -120.193421f;

        assertEquals(142.0f, connectingObjectData.calculateDistanceBetweenCoordinates(startLatitude, startLongitude, endLatitude, endLongitude), 1.0f);
        System.out.println(" pass.");

//        System.out.print("    Seventh test:");
//        startLatitude = -32.820702f;
//        startLongitude = -61.333981f;
//
//        endLatitude = -32.821477f;
//        endLongitude = -61.329550f;
//
//        assertEquals(400.0f, connectingObjectData.calculateDistanceBetweenCoordinates(startLatitude, startLongitude, endLatitude, endLongitude), 1.0f);
//        System.out.println(" pass.");
    }

    @Test
    public void calculateInclination() throws Exception {
        System.out.println("Test for calculation of inclination angle between two GPS coordinates:");
        ConnectingObjectData connectingObjectData = new ConnectingObjectData();

        System.out.print("    First test:");
        float startAltitude = 150.0f; //in meters
        float endAltitude = 250.0f; //in meters
        float distanceBetweenObjects = 65.0f; //in meters

        assertEquals(56.00f, connectingObjectData.calculateInclination(startAltitude, endAltitude, distanceBetweenObjects), 1.0f);
        System.out.println(" pass.");

        System.out.print("    Second test:");
        startAltitude = 1153.0f; //in meters
        endAltitude = 784.0f; //in meters
        distanceBetweenObjects = 237.0f; //in meters

        assertEquals(57.00f, connectingObjectData.calculateInclination(startAltitude, endAltitude, distanceBetweenObjects), 1.0f);
        System.out.println(" pass.");
    }

    @Test
    public void calculateInitRotationForYAxis() throws Exception {
        System.out.println("Test for calculation of init angle around z-axis for object:");
        ConnectingObjectData connectingObjectData = new ConnectingObjectData();

        System.out.print("    First test:");
        float latitudeStart = 49.207990f;
        float longitudeStart = 16.660225f;
        connectingObjectData.setmInitLatitudeStart(latitudeStart);
        connectingObjectData.setmInitLongitudeStart(longitudeStart);

        float latitudeEnd = 49.207089f;
        float longitudeEnd = 16.661384f;
        connectingObjectData.setmInitLatitudeEnd(latitudeEnd);
        connectingObjectData.setmInitLongitudeEnd(longitudeEnd);

        connectingObjectData.setmDistanceBetweenCoordinates((float) connectingObjectData.calculateDistanceBetweenCoordinates(latitudeStart, longitudeStart, latitudeEnd, longitudeEnd));
        assertEquals(50.0f, connectingObjectData.calculateInitRotationForYAxis(), 1.0f);
        System.out.println(" pass.");

        System.out.print("    Second test:");
        connectingObjectData.setmInitLatitudeStart(latitudeEnd);
        connectingObjectData.setmInitLongitudeStart(longitudeEnd);

        connectingObjectData.setmInitLatitudeEnd(latitudeStart);
        connectingObjectData.setmInitLongitudeEnd(longitudeStart);

        connectingObjectData.setmDistanceBetweenCoordinates((float) connectingObjectData.calculateDistanceBetweenCoordinates(latitudeStart, longitudeStart, latitudeEnd, longitudeEnd));
        assertEquals(50.0f, connectingObjectData.calculateInitRotationForYAxis(), 1.0f);
        System.out.println(" pass.");
    }

    @Test
    public void calculateCenterGPSCoordinates() throws Exception {
        System.out.println("Test for calculation GPS coordinates of center between two GPS coordinates:");
        ConnectingObjectData connectingObjectData = new ConnectingObjectData();

        System.out.println("  First test:");
        connectingObjectData.setmInitLatitudeStart(49.207990f);
        connectingObjectData.setmInitLongitudeStart(16.660225f);

        connectingObjectData.setmInitLatitudeEnd(49.207096f);
        connectingObjectData.setmInitLongitudeEnd(16.660225f);

        connectingObjectData.calculateCenterGPSCoordinates();
        System.out.println("      LatitudeS > LatitudeE && LongitudeS == LongitudeE:");
        System.out.print("          Center Latitude:");
        assertEquals(49.207543f, connectingObjectData.getmInitLatitudeCenter(), 0.5f);
        System.out.println(" pass;");
        System.out.print("          Center Longitude:");
        assertEquals(16.660225f, connectingObjectData.getmInitLongitudeCenter(), 0.5f);
        System.out.println(" pass;");

        System.out.println("  Second test:");
        connectingObjectData.setmInitLatitudeStart(49.207096f);
        connectingObjectData.setmInitLongitudeStart(16.660225f);

        connectingObjectData.setmInitLatitudeEnd(49.208000f);
        connectingObjectData.setmInitLongitudeEnd(16.661397f);

        connectingObjectData.calculateCenterGPSCoordinates();
        System.out.println("      LatitudeS < LatitudeE && LongitudeS < LongitudeE:");
        System.out.print("          Center Latitude:");
        assertEquals(49.207548f, connectingObjectData.getmInitLatitudeCenter(), 0.5f);
        System.out.println(" pass;");
        System.out.print("          Center Longitude:");
        assertEquals(16.660811f, connectingObjectData.getmInitLongitudeCenter(), 0.5f);
        System.out.println(" pass;");

        System.out.println("  Third test:");
        connectingObjectData.setmInitAltitudeStart(150.0f);

        connectingObjectData.setmInitAltitudeEnd(1423.0f);

        connectingObjectData.calculateCenterGPSCoordinates();
        System.out.println("      AltitudeS < AltitudeE:");
        System.out.print("          Center Altitude:");
        assertEquals(786.5f, connectingObjectData.getmInitAltitudeCenter(), 0.5f);
        System.out.println(" pass;");
    }

    @Test
    public void determineQuadrantPosition() throws Exception {
        System.out.println("Test of object position around mobile determination:");
        ConnectingObjectData connectingObjectData = new ConnectingObjectData();

        System.out.print("  Object in first quadrant:");
        connectingObjectData.setmInitLatitudeCenter(49.208852f);
        connectingObjectData.setmInitLongitudeCenter(16.662763f);
        connectingObjectData.determineQuadrantPosition(49.208000f, 16.661397f);
        assertEquals(Quadrant.FIRST_QUADRANT, connectingObjectData.getmPosition());
        System.out.println(" pass.");

        System.out.print("  Object in second quadrant:");
        connectingObjectData.setmInitLatitudeCenter(49.208887f);
        connectingObjectData.setmInitLongitudeCenter(16.660097f);
        connectingObjectData.determineQuadrantPosition(49.208000f, 16.661397f);
        assertEquals(Quadrant.SECOND_QUADRANT, connectingObjectData.getmPosition());
        System.out.println(" pass.");

        System.out.print("  Object in third quadrant:");
        connectingObjectData.setmInitLatitudeCenter(49.207103f);
        connectingObjectData.setmInitLongitudeCenter(16.660215f);
        connectingObjectData.determineQuadrantPosition(49.208000f, 16.661397f);
        assertEquals(Quadrant.THIRD_QUADRANT, connectingObjectData.getmPosition());
        System.out.println(" pass.");

        System.out.print("  Object in fourth quadrant:");
        connectingObjectData.setmInitLatitudeCenter(49.207012f);
        connectingObjectData.setmInitLongitudeCenter(16.663954f);
        connectingObjectData.determineQuadrantPosition(49.208000f, 16.661397f);
        assertEquals(Quadrant.FOURTH_QUADRANT, connectingObjectData.getmPosition());
        System.out.println(" pass.");

        System.out.print("  Object on \"y-axis\" between first and second quadrant:");
        connectingObjectData.setmInitLatitudeCenter(49.208852f);
        connectingObjectData.setmInitLongitudeCenter(16.661397f);
        connectingObjectData.determineQuadrantPosition(49.208000f, 16.661397f);
        assertEquals(Quadrant.BETWEEN_FIRST_SECOND_QUADRANT, connectingObjectData.getmPosition());
        System.out.println(" pass.");

        System.out.print("  Object on \"x-axis\" between first and fourth quadrant:");
        connectingObjectData.setmInitLatitudeCenter(49.208000f);
        connectingObjectData.setmInitLongitudeCenter(16.662763f);
        connectingObjectData.determineQuadrantPosition(49.208000f, 16.661397f);
        assertEquals(Quadrant.BETWEEN_FIRST_FOURTH_QUADRANT, connectingObjectData.getmPosition());
        System.out.println(" pass.");

        System.out.print("  Object on \"x-axis\" between second and third quadrant:");
        connectingObjectData.setmInitLatitudeCenter(49.208000f);
        connectingObjectData.setmInitLongitudeCenter(16.555555f);
        connectingObjectData.determineQuadrantPosition(49.208000f, 16.661397f);
        assertEquals(Quadrant.BETWEEN_SECOND_THIRD_QUADRANT, connectingObjectData.getmPosition());
        System.out.println(" pass.");

        System.out.print("  Object on \"y-axis\" between third and fourth quadrant:");
        connectingObjectData.setmInitLatitudeCenter(49.108852f);
        connectingObjectData.setmInitLongitudeCenter(16.661397f);
        connectingObjectData.determineQuadrantPosition(49.208000f, 16.661397f);
        assertEquals(Quadrant.BETWEEN_THIRD_FOURTH_QUADRANT, connectingObjectData.getmPosition());
        System.out.println(" pass.");

    }

    @Test
    public void calculateAbsoluteTransitionXAxis() throws Exception {

    }

    @Test
    public void calculateAngleForCircularCut() throws Exception {
        System.out.println("Test of calculation of angle for circular cat:");
        ConnectingObjectData connectingObjectData = new ConnectingObjectData();
        double gama = 34;
        double[] azimuthValues = {-180, -147, -90, -21, 0, 3, 90, 156, 180};
        double[][] expectedResults = {{236.0f, 203.0f, 146.0, 77.0f, 56.0f, 53.0f, 34.0f, 100.0f, 124.0f},
                {124.0f, 91.0f, 34.0, 35.0f, 56.0f, 59.0f, 146.0f, 212.0f, 236.0f},
                {56.0f, 23.0f, 34.0, 103.0f, 124.0f, 127.0f, 214.0f, 280.0f, 304.0f},
                {304.0f, 271.0f, 214.0, 145.0f, 124.0f, 121.0f, 34.0f, 32.0f, 56.0f},
                {180, 147, 90, 21, 0, 3, 90, 156, 180},
                {270, 237, 180, 111, 90, 87, 0, 66, 90},
                {90, 57, 0, 69, 90, 93, 180, 246, 270},
                {0, 33, 90, 159, 180, 177, 90, 24, 0}};
        int i = 0;

        for (Quadrant position : Quadrant.values()) {
            switch (position) {
                case FIRST_QUADRANT:
                    System.out.println("    First quadrant:");
                    connectingObjectData.setmPosition(Quadrant.FIRST_QUADRANT);
                    break;
                case SECOND_QUADRANT:
                    System.out.println("    Second quadrant:");
                    connectingObjectData.setmPosition(Quadrant.SECOND_QUADRANT);
                    break;
                case THIRD_QUADRANT:
                    System.out.println("    Third quadrant:");
                    connectingObjectData.setmPosition(Quadrant.THIRD_QUADRANT);
                    break;
                case FOURTH_QUADRANT:
                    System.out.println("    Fourth quadrant:");
                    connectingObjectData.setmPosition(Quadrant.FOURTH_QUADRANT);
                    break;
                case BETWEEN_FIRST_SECOND_QUADRANT:
                    System.out.println("    On \"y-axis\" between first and second quadrant:");
                    connectingObjectData.setmPosition(Quadrant.BETWEEN_FIRST_SECOND_QUADRANT);
                    break;
                case BETWEEN_FIRST_FOURTH_QUADRANT:
                    System.out.println("    On \"x-axis\" between first and fourth quadrant:");
                    connectingObjectData.setmPosition(Quadrant.BETWEEN_FIRST_FOURTH_QUADRANT);
                    break;
                case BETWEEN_SECOND_THIRD_QUADRANT:
                    System.out.println("    On \"x-axis\" between second and third quadrant:");
                    connectingObjectData.setmPosition(Quadrant.BETWEEN_SECOND_THIRD_QUADRANT);
                    break;
                case BETWEEN_THIRD_FOURTH_QUADRANT:
                    System.out.println("    On \"y-axis\" between third and fourth quadrant:");
                    connectingObjectData.setmPosition(Quadrant.BETWEEN_THIRD_FOURTH_QUADRANT);
                    break;
                default:
                    return;
            }
            System.out.print("          Azimuth = -180°:");
            assertEquals(expectedResults[position.ordinal()][i], connectingObjectData.calculateAngleForCircularCut(gama, azimuthValues[i]), 0.0f);
            System.out.println(" pass.");
            i++;
            System.out.print("          Azimuth = -147°:");
            assertEquals(expectedResults[position.ordinal()][i], connectingObjectData.calculateAngleForCircularCut(gama, azimuthValues[i]), 0.0f);
            System.out.println(" pass.");
            i++;
            System.out.print("          Azimuth = -90°:");
            assertEquals(expectedResults[position.ordinal()][i], connectingObjectData.calculateAngleForCircularCut(gama, azimuthValues[i]), 0.0f);
            System.out.println(" pass.");
            i++;
            System.out.print("          Azimuth = -21°:");
            assertEquals(expectedResults[position.ordinal()][i], connectingObjectData.calculateAngleForCircularCut(gama, azimuthValues[i]), 0.0f);
            System.out.println(" pass.");
            i++;
            System.out.print("          Azimuth = 0°:");
            assertEquals(expectedResults[position.ordinal()][i], connectingObjectData.calculateAngleForCircularCut(gama, azimuthValues[i]), 0.0f);
            System.out.println(" pass.");
            i++;
            System.out.print("          Azimuth = 3°:");
            assertEquals(expectedResults[position.ordinal()][i], connectingObjectData.calculateAngleForCircularCut(gama, azimuthValues[i]), 0.0f);
            System.out.println(" pass.");
            i++;
            System.out.print("          Azimuth = 90°:");
            assertEquals(expectedResults[position.ordinal()][i], connectingObjectData.calculateAngleForCircularCut(gama, azimuthValues[i]), 0.0f);
            System.out.println(" pass.");
            i++;
            System.out.print("          Azimuth = 156°:");
            assertEquals(expectedResults[position.ordinal()][i], connectingObjectData.calculateAngleForCircularCut(gama, azimuthValues[i]), 0.0f);
            System.out.println(" pass.");
            i++;
            System.out.print("          Azimuth = 180°:");
            assertEquals(expectedResults[position.ordinal()][i], connectingObjectData.calculateAngleForCircularCut(gama, azimuthValues[i]), 0.0f);
            System.out.println(" pass.");
            i = 0;
        }
    }

    @Test
    public void calculateCircularCut() throws Exception {
        System.out.println("Test of distance of circular cut calculation:");
        ConnectingObjectData connectingObjectData = new ConnectingObjectData();

        System.out.print("  First test:");
        assertEquals(14.137167f, connectingObjectData.calculateCircularCut(27.0, 30), 0.0f);
        System.out.println(" pass.");

        System.out.print("  Second test:");
        assertEquals(46.002303f, connectingObjectData.calculateCircularCut(1317.8689, 2), 0.0f);
        System.out.println(" pass.");

        System.out.print("  Third test:");
        assertEquals(8257.413358f, connectingObjectData.calculateCircularCut(1317.8689, 359), 0.0f);
        System.out.println(" pass.");
    }

    @Test
    public void calculateAbsoluteTransitionYAxis() throws Exception {

    }

    @Test
    public void calculateAbsoluteTransitionZAxis() throws Exception {

    }

    @Test
    public void checkVisibility() throws Exception {

    }

    @Test
    public void getmDistanceBetweenCoordinates() throws Exception {

    }

    @Test
    public void setmDistanceBetweenCoordinates() throws Exception {

    }

    @Test
    public void getmInitLongitudeStart() throws Exception {

    }

    @Test
    public void setmInitLongitudeStart() throws Exception {

    }

    @Test
    public void getmInitLatitudeStart() throws Exception {

    }

    @Test
    public void setmInitLatitudeStart() throws Exception {

    }

    @Test
    public void getmInitAltitudeStart() throws Exception {

    }

    @Test
    public void setmInitAltitudeStart() throws Exception {

    }

    @Test
    public void getmInitLatitudeEnd() throws Exception {

    }

    @Test
    public void setmInitLatitudeEnd() throws Exception {

    }

    @Test
    public void getmInitLongitudeEnd() throws Exception {

    }

    @Test
    public void setmInitLongitudeEnd() throws Exception {

    }

    @Test
    public void getmInitAltitudeEnd() throws Exception {

    }

    @Test
    public void setmInitAltitudeEnd() throws Exception {

    }

    @Test
    public void getmInitAltitudeCenter() throws Exception {

    }

    @Test
    public void setmInitAltitudeCenter() throws Exception {

    }

    @Test
    public void getmInitLongitudeCenter() throws Exception {

    }

    @Test
    public void setmInitLongitudeCenter() throws Exception {

    }

    @Test
    public void getmInitLatitudeCenter() throws Exception {

    }

    @Test
    public void setmInitLatitudeCenter() throws Exception {

    }

    @Test
    public void getmInitAngleXAxis() throws Exception {

    }

    @Test
    public void setmInitAngleXAxis() throws Exception {

    }

    @Test
    public void getmInitAngleYAxis() throws Exception {

    }

    @Test
    public void setmInitAngleYAxis() throws Exception {

    }

    @Test
    public void getmInitAngleZAxis() throws Exception {

    }

    @Test
    public void setmInitAngleZAxis() throws Exception {

    }

    @Test
    public void getmTransitionXAxis() throws Exception {

    }

    @Test
    public void setmTransitionXAxis() throws Exception {

    }

    @Test
    public void getmTransitionYAxis() throws Exception {

    }

    @Test
    public void setmTransitionYAxis() throws Exception {

    }

    @Test
    public void getmTransitionZAxis() throws Exception {

    }

    @Test
    public void setmTransitionZAxis() throws Exception {

    }

    @Test
    public void getmRotationXAxis() throws Exception {

    }

    @Test
    public void setmRotationXAxis() throws Exception {

    }

    @Test
    public void getmRotationYAxis() throws Exception {

    }

    @Test
    public void setmRotationYAxis() throws Exception {

    }

    @Test
    public void getmRotationZAxis() throws Exception {

    }

    @Test
    public void setmRotationZAxis() throws Exception {

    }

    @Test
    public void ismIsVisible() throws Exception {

    }

    @Test
    public void setmIsVisible() throws Exception {

    }

}